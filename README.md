
# Overview

### 1. Back

 - very very simple express server, only one file "index.js" (and DB.csv for storage)
 - Should divide  into "search" category, "history" category, and separate DB interface (maybe make mongo)
 - three routes overall, search normal, search persist and get history.

### 2. Front

 - pages includes all components, should organize
 - redux includes "types" for constants and "store" as main redux file
 - as well as "actions" including redux actions and server API functions, should organize
 - and "reducers" with "rootReducer" which composes the reducers (only one in this case "mainReducer")
 - should add selectors to improve preformance
 - styles includes css styles
 
### General

 - To save a term to history, click it in the local history list
 - Clicking a link in the list makes it the search term
 - to make it open the link instead, change comments in lines 62, 63 in fornt3/pages/home
 - The server adds subtopics to the list recursively

# Usage

 - clear ports 3000 and 5000
 - clone repo
 - open two command line prompts

### 1. Back

 - go to server dir `cd ./duckduckgoproxy/back`
 - run npm install `npm i`
 - run the server `npm run test`

### 2. Front

 - go to server dir `cd ./duckduckgoproxy/front3`
 - run npm install `npm i`
 - run the server `npm run dev`

#### Finally

 - open browser to `http://localhost:3000/`