import React, { useState } from 'react';
import  '../styles/publish.css';
import { getPostsAPI, submitPostAPI } from '../actions/searchAPI';
import SideBar from '../components/sideBar';
import { TextField, FormControl, InputLabel, Select, MenuItem } from '@mui/material';
import { LocalizationProvider, DatePicker } from '@mui/lab';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import { defined } from '../utils';

function Publish() {

    // const [loading, setLoading] = useState(false);
    const [notificationText, setNotificationText] = useState('');
    const [post, setPost] = useState({});
    const [errors, setErrors] = useState({});

    const fields = [
        {
            name: 'email',
            label: 'Contact Email',
            type: 'string',
            required: true
        },
        {
            name: 'title',
            label: 'Title',
            type: 'string',
            required: true
        },
        {
            name: 'type',
            label: 'Type',
            type: 'enum',
            enum: [
                'Platform',
                'Blockchain Service'
            ],
            required: true,
            default: 'Platform'
        },
        {
            name: 'description',
            label: 'Description',
            type: 'string',
            multiline: true,
            default: ''
        },
        {
            name: 'isWhitelist',
            label: 'Whitelist',
            type: 'boolean',
            default: ''
        },
        {
            name: 'fundraisingGoal',
            label: 'Fundraising Goal',
            type: 'number'
        },
        {
            name: 'ticker',
            label: 'Ticker',
            type: 'string',
            required: true
        },
        {
            name: 'tokenType',
            label: 'Token Type',
            type: 'string',
            default: 'ERC20'
        },
        {
            name: 'homepage',
            label: 'Homepage',
            type: 'string',
        },
        {
            name: 'videoUrl',
            label: 'Video URL',
            type: 'string',
            required: true
        },
        {
            name: 'icoOrAirdrop',
            label: 'Event Type',
            type: 'enum',
            enum: ['ICO', 'Airdrop'],
            default: 'ICO'
        },
        {
            name: 'startDate',
            label: 'Start Date',
            type: 'date',
            default: Date.now
        },
        {
            name: 'endDate',
            label: 'End Date',
            type: 'date',
            default: Date.now
        },
    ];

    const handleSubmit = () => {
        const errorsObj = {};
        let shouldSubmit = true;
        for (let field of fields) {
            if (field.required && !defined(post[field.name]) && !defined(field.default)) {
                errorsObj[field.name] = 'This field is required to submit';
                shouldSubmit = false;
            }
        }
        if (shouldSubmit) {
            submitPostAPI({
                    ...post
                }, () => {
                    setNotificationText('Submitting post...');
                }, (res) => {
                    setNotificationText(`Post: ${post.title} - submitted for review`);
                    console.log('res', res);
                }
            );
        } else {
            setErrors(errorsObj);
        }
    }

    const handleChange = (val, fieldName) => {
        setPost({ ...post, [fieldName]: val });
        if (errors[fieldName] && val) {
            const newErrors = { ...errors };
            delete newErrors[fieldName];
            setErrors(newErrors);
        }
    }

    const _field = (field) => {
        const inputId = `${field.name}_input`;
        const inputLabelId = `${field.name}_input_label`;
        const showText = field.label || field.name;
        const value = post[field.name] || field.default;
        switch (field.type) {
            case 'string':
                return (
                    <TextField
                        error={!!errors[field.name]}
                        key={inputId}
                        id={inputId}
                        label={showText}
                        required={field.required}
                        multiline={field.multiline}
                        variant='filled'
                        margin='normal'
                        fullWidth
                        value={value}
                        onChange={(event) => {
                            handleChange(event.target.value, field.name);
                        }}
                        helperText={errors[field.name]}
                    />
                );
            case 'enum':
                return (
                    <FormControl
                        // sx={{ m: 1, minWidth: 120 }}
                        variant='filled'
                        margin='normal'
                        fullWidth
                    >
                        <InputLabel id={inputLabelId}>{showText}</InputLabel>
                        <Select
                            labelId={inputLabelId}
                            id={inputId}
                            value={value}
                            label={showText}
                            onChange={(event) => {
                                handleChange(event.target.value, field.name);
                            }}
                        >
                            <MenuItem key={`${inputId}_none_option`} value=''>
                                <em>None</em>
                            </MenuItem>
                            {field.enum.map((op) =>
                                <MenuItem key={`${op}_option`} value={op}>{op}</MenuItem>
                            )}
                        </Select>
                        {/* <FormHelperText>Disabled</FormHelperText> */}
                    </FormControl>
                );
            case 'date':
                return (
                    <div style={{ margin: 8 }}>
                        <LocalizationProvider dateAdapter={AdapterDateFns}>
                            <DatePicker
                                margin='normal'
                                label={showText}
                                value={value}
                                onChange={(newValue) => {
                                    console.log('newValue', newValue);
                                    handleChange(newValue, field.name);
                                }}
                                renderInput={(params) => <TextField {...params} />}
                            />
                        </LocalizationProvider>
                    </div>
                );
            default:
                return null;
        }
    }

    const _main = () => {
        return (
            <div className={'publishInputContainer'}>
                {fields.map(field => (
                    <div
                        key={`${field.name}_input_container`}
                        style={{ width: '100%' }}
                    >
                        {_field(field)}
                    </div>
                ))}
                <div
                    className="sButton"
                    style={{ marginTop: 20 }}
                    onClick={handleSubmit}
                >
                    <span style={{ fontSize: 14 }}>Submit</span>
                </div>
            </div>
        );
    };

    return (
        <div className={'topContainer'}>
            <SideBar />
            <div className={'publishMainContainer'}>
                <div className={'publishTitle'}>
                    Publish Funding Campaign
                </div>
                <div className='notificationText'>
                    {notificationText}
                </div>
                {_main()}
            </div>
        </div>
    );
}


export default Publish;