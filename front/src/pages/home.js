import React, { useEffect, useState } from 'react';
import ReactPlayer from 'react-player';
import  '../styles/home.css';
import { getPostsAPI } from '../actions/searchAPI';
import Search from '../components/search';
import SideBar from '../components/sideBar';

function Home() {

    const [loading, setLoading] = useState(false);
    const [posts, setPosts] = useState([]);

    const [hoveredPost, setHoveredPost] = useState(null);
    const [selectedPost, setSelectedPost] = useState(null);

    useEffect(() => {
        getPostsAPI(
            () => {
                setLoading(true);
            },
            (res) => {
                setPosts(res.data.data);
                setLoading(false);
            }
        );
    }, [setLoading, setPosts]);

    // useEffect(() => {
    //     console.log('history', history.location);
    // }, [history]);

    const _post = (post) => (
        <div
            key={post._id}
            className={'postContainer'}
            onMouseEnter={() => setHoveredPost(post._id)}
            onMouseLeave={() => setHoveredPost(null)}
            onClick={() => {
                setSelectedPost(post.id);
            }}
        >
            <ReactPlayer
                playing={hoveredPost === post._id}
                height={313}
                width={313}
                url={post.videoUrl}
                muted={true}
                controls={false}
            />
            <div className={'boxICOTextSection'}>
                <div className={'boxICOTitle'}>{post.title}</div>
                <div>{post.type}</div>
                <div>Fundraising Goal: {
                    post.fundraisingGoal === undefined ?
                    'NOT SET' : post.fundraisingGoal
                }</div>
                <div className={'boxICODescriptionHolder'}>{post.description}</div>
            </div>
        </div>
    );
    console.log('posts', posts);

    const _main = () => {
        return (
            <div className={'mainContainer'}>
                <Search />
                <div className={'postsContainer'}>
                    {posts.map(post => _post(post))}
                </div>
            </div>
        );
    };

    return (
        <div className={'topContainer'}>
            <SideBar />
            {_main()}
        </div>
    );
}


export default Home;